package com.wesam.tasty.data.api

import com.wesam.tasty.data.model.FoodModel
import retrofit2.Response
import javax.inject.Inject

class ApiRepository @Inject constructor(private val apiService: ApiService) : ApiRepo {
    override suspend fun getFoodInfo(tage: String,from: String,size: String,q: String): Response<FoodModel> =
        apiService.getFoodInfo(tage,from,size,q)

}