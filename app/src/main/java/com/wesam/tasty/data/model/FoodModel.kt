package com.wesam.tasty.data.model


import com.squareup.moshi.Json
import com.wesam.tasty.data.model.FoodData

data class FoodModel(
    @Json(name = "count")
    var count: Int? = 0,

    //var results: List<FoodData>? = listOf(),
    @Json(name = "results")
    var results: List<FoodEntity>? = listOf()
)