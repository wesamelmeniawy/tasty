package com.wesam.tasty.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity (tableName = "food_table")
data class FoodEntity(
    @Json(name = "id") @PrimaryKey @ColumnInfo(name = "id") val id:Int?,
    @Json(name = "name") @ColumnInfo(name = "name") val name:String?,
    @Json(name = "thumbnail_url") @ColumnInfo(name = "image_url") val image_url:String?,
    @Json(name = "description") @ColumnInfo(name = "description") val description:String?,
    @Json(name = "country") @ColumnInfo(name = "country") val country:String?,
    @Json(name = "video_url") @ColumnInfo(name = "video_url") val video_url:String?,
    @ColumnInfo(name = "favorite") val favorite:Boolean?,
    @ColumnInfo(name = "type") val type:String?,
    @Json(name = "sections") @ColumnInfo(name = "section") val section : List<Section>? = listOf(),
    @Json(name = "instructions") @ColumnInfo(name = "instructions") val instructions : List<Instruction>? = listOf()
)