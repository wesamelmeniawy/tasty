package com.wesam.tasty.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Instruction(
    @Json(name = "display_text")
    var displayText: String? = ""
)