package com.wesam.tasty.data.api

import com.wesam.tasty.data.model.FoodModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("recipes/list")
    suspend fun getFoodInfo(@Query("tages") tages: String,
                           @Query("from") from: String,
                           @Query("size") size: String,
                           @Query("q") q: String): Response<FoodModel>
}