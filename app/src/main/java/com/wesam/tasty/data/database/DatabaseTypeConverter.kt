package com.wesam.tasty.data.database

import androidx.room.TypeConverter
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Types
import com.wesam.tasty.data.model.Instruction
import com.wesam.tasty.data.model.Section
import com.wesam.tasty.utils.Constants

class DatabaseTypeConverter {
    private val moshi = Constants.moshi
    private val sectionListType = Types.newParameterizedType(List::class.java, Section::class.java)
    private val adapterSectionList: JsonAdapter<List<Section>> = moshi.adapter(sectionListType)
    private val instructionListType = Types.newParameterizedType(List::class.java, Instruction::class.java)
    private val adapterInstructionList: JsonAdapter<List<Instruction>> = moshi.adapter(instructionListType)


    @TypeConverter
    fun stringToSectionList(value: String): List<Section> = adapterSectionList.fromJson(value) ?: listOf()

    @TypeConverter
    fun sectionListToString(list: List<Section>): String = adapterSectionList.toJson(list) ?: ""

    @TypeConverter
    fun stringToInstructionList(value: String): List<Instruction> = adapterInstructionList.fromJson(value) ?: listOf()

    @TypeConverter
    fun instructionListToString(list: List<Instruction>): String = adapterInstructionList.toJson(list) ?: ""
}