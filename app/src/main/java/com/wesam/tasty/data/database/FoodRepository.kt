package com.wesam.tasty.data.database

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.wesam.tasty.data.model.FoodEntity

class FoodRepository (private val foodDao: FoodDao){
    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allFood: LiveData<List<FoodEntity>> = foodDao.getAlphabetizedWords()
    val favoriteFood : LiveData<List<FoodEntity>> = foodDao.favoriteList()

    // You must call this on a non-UI thread or your app will crash. So we're making this a
    // suspend function so the caller methods know this.
    // Like this, Room ensures that you're not doing any long running operations on the main
    // thread, blocking the UI.
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(foodEntity: FoodEntity) {
        foodDao.insert(foodEntity)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun update(id:Int,favorite:Boolean){
        foodDao.update(id,favorite)
    }


}