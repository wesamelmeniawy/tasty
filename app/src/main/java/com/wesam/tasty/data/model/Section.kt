package com.wesam.tasty.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Section(
    @Json(name = "components")
    var components: List<Component>? = listOf()
)