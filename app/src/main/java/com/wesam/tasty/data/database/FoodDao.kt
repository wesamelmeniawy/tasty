package com.wesam.tasty.data.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.wesam.tasty.data.model.FoodEntity

@Dao
interface FoodDao{
    @Query("SELECT * from food_table")
    fun getAlphabetizedWords(): LiveData<List<FoodEntity>>

    @Query("SELECT * from food_table WHERE favorite = 1 ")
    fun favoriteList(): LiveData<List<FoodEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(foodEntity: FoodEntity)


    @Query("UPDATE food_table SET favorite = :favoriteFood WHERE id = :foodId")
    fun update(foodId:Int,favoriteFood:Boolean)

}

