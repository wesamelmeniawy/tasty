package com.wesam.tasty.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.wesam.tasty.R
import com.wesam.tasty.data.model.FoodEntity
import com.wesam.tasty.databinding.SearchRowItemBinding
import javax.inject.Inject

class SearchAdapter @Inject constructor(
    private val requestManager: RequestManager,
    val listener: ItemClickedListener
) :
    RecyclerView.Adapter<SearchAdapter.SearchAdapterViewHolder>() {
    private var data: List<FoodEntity> = ArrayList()

    interface ItemClickedListener {
        fun onItemClickListener(position: Int)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchAdapter.SearchAdapterViewHolder {
        val foodItemBinding =
            DataBindingUtil.inflate<SearchRowItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.search_row_item,
                parent,
                false
            )
        return SearchAdapterViewHolder(foodItemBinding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: SearchAdapter.SearchAdapterViewHolder, position: Int) {
        holder.binding.nameText.text = data[position].name
        holder.binding.countryName.text = "Country: " + data[position].country.toString().toLowerCase()
        holder.binding.type.text = data[position].type
        requestManager
            .load(data[position].image_url)
            .into(holder.binding.foodImage)

    }

    inner class SearchAdapterViewHolder(var binding: SearchRowItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            val position = adapterPosition
            listener.onItemClickListener(position)
        }

    }

    fun loadData(foodData: List<FoodEntity>) {
        this.data = foodData
        notifyDataSetChanged()
    }

}
