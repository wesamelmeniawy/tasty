package com.wesam.tasty.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.wesam.tasty.R
import com.wesam.tasty.data.model.FoodEntity
import com.wesam.tasty.databinding.FoodRowItemBinding
import javax.inject.Inject

class HomeAdapter @Inject constructor(
    private val requestManager: RequestManager,
    val listener: ItemClickedListener,foodType:String
) : RecyclerView.Adapter<HomeAdapter.HomeAdapterViewHolder>() {
    private var data: List<FoodEntity> = ArrayList()

    private val type = foodType

    interface ItemClickedListener {
        fun onItemClickListener(position: Int,type:String)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): HomeAdapter.HomeAdapterViewHolder {
        val foodItemBinding =
            DataBindingUtil.inflate<FoodRowItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.food_row_item,
                parent,
                false
            )
        return HomeAdapterViewHolder(foodItemBinding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: HomeAdapter.HomeAdapterViewHolder, position: Int) {
        holder.binding.foodName.text = data[position].name
        requestManager
            .load(data[position].image_url)
            .into(holder.binding.foodImage)

    }

    inner class HomeAdapterViewHolder(var binding: FoodRowItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            val position = adapterPosition
            listener.onItemClickListener(position,type)
        }
    }

    fun loadData(foodData: List<FoodEntity>) {
        this.data = foodData
        notifyDataSetChanged()
    }

}