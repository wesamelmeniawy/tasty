package com.wesam.tasty.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.wesam.tasty.R
import com.wesam.tasty.data.model.FoodEntity
import com.wesam.tasty.databinding.ViewMoreRowItemBinding
import javax.inject.Inject

class FoodAdapter @Inject constructor(
    private val requestManager: RequestManager,
    val listener: ItemClickedListener
) :
    RecyclerView.Adapter<FoodAdapter.ViewMoreAdapterViewHolder>() {
    private var data: List<FoodEntity> = ArrayList()

    interface ItemClickedListener {
        fun onItemClickListener(position: Int)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FoodAdapter.ViewMoreAdapterViewHolder {
        val foodItemBinding =
            DataBindingUtil.inflate<ViewMoreRowItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.view_more_row_item,
                parent,
                false
            )
        return ViewMoreAdapterViewHolder(foodItemBinding)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: FoodAdapter.ViewMoreAdapterViewHolder, position: Int) {
        holder.binding.foodName.text = data[position].name
        requestManager
            .load(data[position].image_url)
            .into(holder.binding.foodImage)

    }

    inner class ViewMoreAdapterViewHolder(var binding: ViewMoreRowItemBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        init {
            binding.details.setOnClickListener(this)
        }

        override fun onClick(view: View?) {
            val position = adapterPosition
            listener.onItemClickListener(position)
        }

    }

    fun loadData(foodData: List<FoodEntity>) {
        this.data = foodData
        notifyDataSetChanged()
    }

}
