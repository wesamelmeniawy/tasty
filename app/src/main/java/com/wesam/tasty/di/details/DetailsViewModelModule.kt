package com.wesam.tasty.di.details

import androidx.lifecycle.ViewModel
import com.wesam.tasty.di.ViewModelKey
import com.wesam.tasty.ui.details.DetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DetailsViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    abstract fun bindDetailsViewModel(viewModel: DetailsViewModel): ViewModel
}