package com.wesam.tasty.di.viewmore

import androidx.lifecycle.ViewModel
import com.wesam.tasty.di.ViewModelKey
import com.wesam.tasty.ui.viewmore.ViewMoreViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewMoreViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ViewMoreViewModel::class)
    abstract fun bindViewMoreViewModel(viewModel: ViewMoreViewModel): ViewModel

}