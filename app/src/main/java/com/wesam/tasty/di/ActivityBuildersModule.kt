package com.wesam.tasty.di

import com.wesam.tasty.di.details.DetailsViewModelModule
import com.wesam.tasty.di.main.MainFragmentBuildersModule
import com.wesam.tasty.di.main.MainViewModelModule
import com.wesam.tasty.di.viewmore.ViewMoreViewModelModule
import com.wesam.tasty.ui.MainActivity
import com.wesam.tasty.ui.details.DetailsActivity
import com.wesam.tasty.ui.viewmore.ViewMoreActivity
import com.wesam.tasty.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector(
        modules = [
            MainViewModelModule::class,
            MainFragmentBuildersModule::class
        ]
    )
    abstract fun contributeMainActivity(): MainActivity


    @ContributesAndroidInjector(
        modules = [
            ViewMoreViewModelModule::class]
    )
    abstract fun contributeViewMoreActivity(): ViewMoreActivity

    @ContributesAndroidInjector(
        modules = [
            DetailsViewModelModule::class]
    )
    abstract fun contributeDetailsActivity(): DetailsActivity
}