package com.wesam.tasty.ui.details

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Build
import android.os.Build.VERSION.SDK_INT
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.RequestManager
import com.google.android.exoplayer2.DefaultLoadControl
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.util.Util.getUserAgent
import com.squareup.moshi.JsonAdapter
import com.wesam.tasty.R
import com.wesam.tasty.data.model.Component
import com.wesam.tasty.data.model.FoodEntity
import com.wesam.tasty.databinding.ActivityDetailsBinding
import com.wesam.tasty.utils.Constants
import com.wesam.tasty.viewmodels.ViewModelProviderFactory
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject



class DetailsActivity : DaggerAppCompatActivity(){

    private lateinit var binding: ActivityDetailsBinding
    private lateinit var viewModel: DetailsViewModel


    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

    @Inject
    lateinit var requestManager: RequestManager


    private lateinit var foodDetails: FoodEntity
    private var foodList: List<FoodEntity> = arrayListOf()

    private lateinit var player: ExoPlayer


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_details)
        viewModel = ViewModelProvider(this, providerFactory).get(DetailsViewModel::class.java)


        observeViewModel()
        setDetails()
        buttonsClicked()


    }

    /**
     *  Observe ViewModel
     *
     */
    private fun observeViewModel() {
        observeGetDataFromDatabase()
    }

    /**
     * Observe ViewModel to Get Data From Room Database
     *
     */
    private fun observeGetDataFromDatabase() {
        viewModel.allFoods.observe(this, Observer { Foods ->
            Foods?.let {
                foodList = it
            }
        })
    }

    /**
     * Get details from intent and set it to layout component
     *
     */
    @SuppressLint("SetTextI18n")
    private fun setDetails() {
        val details = intent.getStringExtra("food_details")
        val jsonAdapter: JsonAdapter<FoodEntity> = Constants.moshi.adapter(FoodEntity::class.java)
        foodDetails = jsonAdapter.fromJson(details ?: "")!!

        binding.textName.text = "Name: " + foodDetails.name
        //Log.e("foodData",foodDetails.toString())

        checkFavorite()
        setIngredientData()
        setInstructionData()
        handlePlayingVideo()



    }

    /**
     * Update room database to set favorite value if item found
     * else insert item with favorite value to room database
     *
     * @param foodList
     */
    private fun handelSetFavoriteData(foodList: List<FoodEntity>) {
        if(foodList.isNotEmpty()) {
            for (i in foodList) {
                if (i.id == foodDetails.id) {
                    i.id?.let { viewModel.update(it, true) }
                    //Log.e("item", "update")
                    //Log.e("item", i.id.toString())
                    //Log.e("item", foodDetails.id.toString())
                    break
                } else {
                    viewModel.insert(foodDetails)
                   // Log.e("item", "not Found")
                }
            }
            foodDetails.id?.let { viewModel.update(it, true)}
        }
        viewModel.insert(foodDetails)
        foodDetails.id?.let { viewModel.update(it, true) }
    }

    /**
     * Update room database to set  not favorite value
     *
     * @param foodList
     */
    private fun handelSetNotFavoriteData(foodList: List<FoodEntity>) {
        for (i in foodList) {
            if (i.id == foodDetails.id) {
                i.id?.let { viewModel.update(it, false) }
                //Log.e("item", "update")
                //Log.e("item", i.id.toString())
                //Log.e("item", foodDetails.id.toString())
                break
            }
        }
    }

    /**
     * Change button color when clicked and call handelData function
     *
     */
    private fun buttonsClicked() {
        binding.favoriteButton.setOnClickListener {
            binding.favoriteButtonRed.visibility = View.VISIBLE
            binding.favoriteButton.visibility = View.GONE
            handelSetFavoriteData(foodList)
        }
        binding.favoriteButtonRed.setOnClickListener {
            binding.favoriteButton.visibility = View.VISIBLE
            binding.favoriteButtonRed.visibility = View.GONE
            handelSetNotFavoriteData(foodList)
        }
    }

    /**
     * check favorite column to set favorite button
     *
     */
    private fun checkFavorite() {
        if (foodDetails.favorite == true) {
            binding.favoriteButtonRed.visibility = View.VISIBLE
            binding.favoriteButton.visibility = View.GONE
        }

    }

    /**
     * Handel ingredient data which get from bundle
     * and display it if data found
     *
     */
    private fun setIngredientData(){
        val sections = foodDetails.section
        val ingredient: MutableList<Component> = mutableListOf()
        if (sections != null && sections.isNotEmpty()) {
            for (i in sections) {
                i.components?.let {
                    ingredient += it
                }
            }

            val ingredientList: MutableList<String> = mutableListOf()
            for (j in ingredient) {
                j.rawText?.let { ingredientList.add(it)}
            }

            val gradientsString = ingredientList.joinToString(separator = "\n-  " ,prefix = "-  ")
            binding.textIngredient.text = gradientsString
            binding.ingredients.visibility = View.VISIBLE

        }
    }

    /**
     * Handel instructions data which get from bundle
     * and display it if data found
     *
     */
    private fun setInstructionData(){
        val instruction = foodDetails.instructions
        val instructionsListOfString: MutableList<String> = mutableListOf()
        if (instruction != null && instruction.isNotEmpty()) {
            for (i in instruction) {
                i.displayText?.let {
                    instructionsListOfString += it
                    //Log.e("instructions",instructionsListOfString.toString())
                }
            }
            val instructionsString = instructionsListOfString.joinToString(separator = "\n-  " ,prefix = "-  ")
            binding.textInstruction.text = instructionsString
            binding.instructions.visibility = View.VISIBLE
        }
    }

    /**
     * Set media player to video
     *
     */
    private fun handlePlayingVideo(){
        if (foodDetails.video_url != null && foodDetails.video_url != "") {
            binding.videoView.visibility = View.VISIBLE
            initializePlayer()
        } else {
            binding.imageView.visibility = View.VISIBLE
            requestManager
                .load(foodDetails.image_url)
                .into(binding.imageView)
        }
    }

    /**
     * Prepare exoPlayer and set uri of video
     *
     */
    private fun initializePlayer() {
        val trackSelector = DefaultTrackSelector()
        val loadControl = DefaultLoadControl()
        val renderersFactory = DefaultRenderersFactory(this)

        player = ExoPlayerFactory.newSimpleInstance(
           this, renderersFactory, trackSelector, loadControl)

        foodDetails.video_url?.let { play(it) }
        binding.videoView.player = player
    }

    /**
     * Set media source to exoPlayer
     *
     * @param url
     */
   private  fun play(url: String) {
        //1
        val userAgent = getUserAgent(this, this.getString(R.string.app_name))
        //2
        val mediaSource = HlsMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
            .createMediaSource(Uri.parse(url))
        //3
        player.prepare(mediaSource)
        //4
        player.playWhenReady = true
    }

    /**
     * Stop playing video
     *
     */
    private fun releasePlayer() {
        player.stop()
        player.release()
    }
    override fun onPause() {
        super.onPause()
        if (SDK_INT < Build.VERSION_CODES.N && foodDetails.video_url!=null
            && foodDetails.video_url != "") {
            releasePlayer()
        }
    }
    override fun onStop() {
        super.onStop()
        if (SDK_INT >= Build.VERSION_CODES.N && foodDetails.video_url!=null
            && foodDetails.video_url != "") {
            releasePlayer()
        }
    }
}