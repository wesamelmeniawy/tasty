package com.wesam.tasty.ui.home

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.wesam.tasty.data.model.FoodModel
import com.wesam.tasty.data.api.ApiRepository
import com.wesam.tasty.data.database.FoodRepository
import com.wesam.tasty.data.database.FoodRoomDatabase
import com.wesam.tasty.data.model.FoodEntity
import kotlinx.coroutines.*
import javax.inject.Inject


class HomeViewModel @Inject constructor(private val apiRepository: ApiRepository,application: Application) :ViewModel(){

    private val _dinnerModel = MutableLiveData<FoodModel>()

    val foodModel: LiveData<FoodModel>
        get() = _dinnerModel

    private val _dessertModel = MutableLiveData<FoodModel>()

    val dessertModel: LiveData<FoodModel>
        get() = _dessertModel

    private val _healthyModel = MutableLiveData<FoodModel>()

    val healthyModel: LiveData<FoodModel>
        get() = _healthyModel

    private val _fastFoodModel = MutableLiveData<FoodModel>()

    val fastFoodModel: LiveData<FoodModel>
        get() = _fastFoodModel



    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.IO)

    private val repository: FoodRepository
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allFoods: LiveData<List<FoodEntity>>

    init {
        val foodsDao = FoodRoomDatabase.getDatabase(application, coroutineScope).foodDao()
        repository = FoodRepository(foodsDao)
        allFoods = repository.allFood
    }


    /**
     * Launching a new coroutine to get dinner data
     *
     * @param tages
     * @param from
     * @param size
     * @param q
     */

    fun dinnerInfo(tages: String,from: String,size: String,q: String) {
        coroutineScope.launch {
            try {
                val response = apiRepository.getFoodInfo(tages,from,size,q)
                when (response.code()) {
                    200 -> {
                        _dinnerModel.postValue(response.body())
                    }else ->{
                    _dinnerModel.postValue(null)
                }
                }
            } catch (e: Exception) {
                Log.e("HomeViewModelDinner",e.toString())
                _dinnerModel.postValue(null)
            }
        }
    }

    /**
     * Launching a new coroutine to get dessert data
     *
     * @param tages
     * @param from
     * @param size
     * @param q
     */

    fun dessertInfo(tages: String,from: String,size: String,q: String) {
        coroutineScope.launch {
            try {
                val response = apiRepository.getFoodInfo(tages,from,size,q)
                when (response.code()) {
                    200 -> {
                        _dessertModel.postValue(response.body())
                    }else ->{
                    _dessertModel.postValue(null)
                }
                }
            } catch (e: Exception) {
                Log.e("HomeViewModelDessert",e.toString())
                _dessertModel.postValue(null)
            }
        }
    }

    /**
     * Launching a new coroutine to get healthy food data
     *
     * @param tages
     * @param from
     * @param size
     * @param q
     */

    fun healthyFoodInfo(tages: String,from: String,size: String,q: String) {
        coroutineScope.launch {
            try {
                val response = apiRepository.getFoodInfo(tages,from,size,q)
                when (response.code()) {
                    200 -> {
                        _healthyModel.postValue(response.body())
                    }else ->{
                    _healthyModel.postValue(null)
                }
                }
            } catch (e: Exception) {
                Log.e("HomeViewModelHealthy",e.toString())
                _healthyModel.postValue(null)
            }
        }

    }
    /**
     *  Launching a new coroutine to get Fast Food data
     *
     * @param tages
     * @param from
     * @param size
     * @param q
     */

    fun fastFoodInfo(tages: String,from: String,size: String,q: String) {
        coroutineScope.launch {
            try {
                val response = apiRepository.getFoodInfo(tages,from,size,q)
                when (response.code()) {
                    200 -> {
                        _fastFoodModel.postValue(response.body())
                    }else ->{
                    _fastFoodModel.postValue(null)
                }
                }
            } catch (e: Exception) {
                Log.e("HomeViewModelFast",e.toString())
                _fastFoodModel.postValue(null)
            }
        }
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(foodEntity: FoodEntity) = coroutineScope.launch(Dispatchers.IO) {
        repository.insert(foodEntity)
    }


    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}