package com.wesam.tasty.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.transition.AutoTransition
import android.transition.TransitionManager
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.wesam.tasty.R
import com.wesam.tasty.databinding.ActivityMainBinding
import com.wesam.tasty.ui.favorite.FavoriteFragment
import com.wesam.tasty.ui.home.HomeFragment
import com.wesam.tasty.ui.search.SearchFragment
import androidx.fragment.app.FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
import com.wesam.tasty.utils.ViewPagerAdapter
import dagger.android.DaggerActivity
import dagger.android.support.DaggerAppCompatActivity


class MainActivity : DaggerAppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)

        itemClicked()
        setupViewPager(binding.viewPager)
        viewPagerPageChangeListener()
    }

    /**
     * select
     * @param id
     */
    private fun select(id: Int) {
        val autoTransition = AutoTransition()
        autoTransition.duration = 150
        TransitionManager.beginDelayedTransition(binding.bottomBar,autoTransition)
        val cs = ConstraintSet()
        cs.clone(binding.homeAction)
        if (id == R.id.home_action) {
            DrawableCompat.setTint(
                binding.homeAction.background, ContextCompat.getColor(
                    this,
                    R.color.navigation_color
                )
            )
            cs.setVisibility(binding.homeIconText.id, ConstraintSet.VISIBLE)
            binding.viewPager.currentItem = 0
        } else {
            DrawableCompat.setTint(
                binding.homeAction.background,
                ContextCompat.getColor(this, android.R.color.transparent)
            )
            cs.setVisibility(binding.homeIconText.id, ConstraintSet.GONE)
        }
        cs.applyTo(binding.homeAction)


        cs.clone(binding.searchAction)
        if (id == R.id.search_action) {
            DrawableCompat.setTint(
                binding.searchAction.background, ContextCompat.getColor(
                    this,
                    R.color.navigation_color
                )
            )
            cs.setVisibility(binding.searchIconText.id, ConstraintSet.VISIBLE)
            binding.viewPager.currentItem = 1
        } else {
            DrawableCompat.setTint(
                binding.searchAction.background,
                ContextCompat.getColor(this, android.R.color.transparent)
            )
            cs.setVisibility(binding.searchIconText.id, ConstraintSet.GONE)
        }
        cs.applyTo(binding.searchAction)

        cs.clone(binding.favoriteAction)
        if (id == R.id.favorite_action) {
            DrawableCompat.setTint(
                binding.favoriteAction.background, ContextCompat.getColor(
                    this,
                    R.color.navigation_color
                )
            )
            cs.setVisibility(binding.favoriteIconText.id, ConstraintSet.VISIBLE)
            binding.viewPager.currentItem = 2
        } else {
            DrawableCompat.setTint(
                binding.favoriteAction.background,
                ContextCompat.getColor(this, android.R.color.transparent)
            )
            cs.setVisibility(binding.favoriteIconText.id, ConstraintSet.GONE)
        }
        cs.applyTo(binding.favoriteAction)
    }
    /**
     * setupViewPager
     * @param viewpager
     */
    private fun setupViewPager(viewpager: ViewPager) {
        val adapter = ViewPagerAdapter(
            supportFragmentManager
        )

        adapter.addFragment(HomeFragment(), "Home")
        adapter.addFragment(SearchFragment(), "Search")
        adapter.addFragment(FavoriteFragment(), "Favorite")
        viewpager.adapter = adapter
    }

    /**
     * viewPagerPageChangeListener
     */
    private fun viewPagerPageChangeListener() {
        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> select(R.id.home_action)
                    1 -> select(R.id.search_action)
                    2 -> select(R.id.favorite_action)
                }
            }
        })

    }

    /**
     * itemClicked
     */
    private fun itemClicked() {
        binding.homeAction.setOnClickListener { select(R.id.home_action) }
        binding.searchAction.setOnClickListener { select(R.id.search_action) }
        binding.favoriteAction.setOnClickListener { select(R.id.favorite_action) }
    }
}