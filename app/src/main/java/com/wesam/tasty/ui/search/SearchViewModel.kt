package com.wesam.tasty.ui.search

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.wesam.tasty.data.api.ApiRepository
import com.wesam.tasty.data.model.FoodModel
import kotlinx.coroutines.*
import javax.inject.Inject

class SearchViewModel @Inject constructor(private val apiRepository: ApiRepository) : ViewModel(){
    private val _searchFoodModel = MutableLiveData<FoodModel>()

    val searchFoodModel: LiveData<FoodModel>
        get() = _searchFoodModel

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.IO)

    /**
     * Launching a new coroutine to get food  data from search
     *
     * @param tages
     * @param from
     * @param size
     * @param name
     */

    fun foodInfo(tages: String,from: String,size: String,name: String) {
        coroutineScope.launch {
            try {
                val response = apiRepository.getFoodInfo(tages,from,size,name)
                when (response.code()) {
                    200 -> {
                        _searchFoodModel.postValue(response.body())
                    }else ->{
                    _searchFoodModel.postValue(null)
                }
                }
            } catch (e: Exception) {
                Log.e("SearchViewModel",e.toString())
                _searchFoodModel.postValue(null)
            }
        }
    }

}