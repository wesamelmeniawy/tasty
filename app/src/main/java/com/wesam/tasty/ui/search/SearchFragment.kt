package com.wesam.tasty.ui.search

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo.IME_ACTION_SEARCH
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.squareup.moshi.JsonAdapter
import com.wesam.tasty.R
import com.wesam.tasty.adapter.SearchAdapter
import com.wesam.tasty.data.model.FoodEntity
import com.wesam.tasty.databinding.FragmentSearchBinding
import com.wesam.tasty.ui.details.DetailsActivity
import com.wesam.tasty.utils.Constants
import com.wesam.tasty.utils.Utils
import com.wesam.tasty.viewmodels.ViewModelProviderFactory
import dagger.android.support.DaggerFragment
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 *
 */
class SearchFragment : DaggerFragment(), SearchAdapter.ItemClickedListener {

    private lateinit var binding: FragmentSearchBinding

    private lateinit var viewModel: SearchViewModel


    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

    @Inject
    lateinit var requestManager: RequestManager

    private lateinit var searchAdapter: SearchAdapter

    private var searchList: List<FoodEntity> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSearchBinding.inflate(inflater)
        viewModel = ViewModelProvider(this, providerFactory).get(SearchViewModel::class.java)

        editTextSearchListener()
        editTextListener()
        buttonClicked()
        handelRecyclerview()
        observeViewModel()

        return binding.root
    }

    /**
     *  Observe ViewModel
     *
     */
    private fun observeViewModel() {
        observeGetFoodData()
    }


    /**
     * Observe ViewMode to Get search Food Data , set it in adapter
     *
     */
    private fun observeGetFoodData() {
        viewModel.searchFoodModel.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if (it.count != 0) {
                    binding.searchRecyclerview.visibility = View.VISIBLE
                    searchList = it.results!!
                    searchAdapter.loadData(searchList)

                } else {
                    binding.noResultText.visibility = View.VISIBLE
                    binding.progress.visibility = View.GONE
                }

            } else {
                //Log.e("null", "null")
                binding.progress.visibility = View.GONE
            }
            binding.progress.visibility = View.GONE
        })
    }



    /**
     * Handel recyclerview for setting adapter and layout
     *
     */
    private fun handelRecyclerview() {
        val dinnerLayoutManage = LinearLayoutManager(
            context, LinearLayoutManager.VERTICAL,
            false
        )
        searchAdapter = SearchAdapter(requestManager, this)
        binding.searchRecyclerview.layoutManager = dinnerLayoutManage
        binding.searchRecyclerview.adapter = searchAdapter
    }

    /**
     * Open activity details when item clicked from recyclerview
     * for showing more details about this item
     *
     * @param position
     */
    override fun onItemClickListener(position: Int) {
        val intent = Intent(context, DetailsActivity::class.java)
        val jsonAdapter: JsonAdapter<FoodEntity> = Constants.moshi.adapter(FoodEntity::class.java)
        intent.putExtra("food_details", jsonAdapter.toJson(searchList[position]))
        startActivity(intent)
    }

    /**
     * Listen for action editor listener
     * if search action clicked  service called and  keyboard hided
     *
     */
    private fun editTextSearchListener(){
        binding.searchEditText.setOnEditorActionListener(OnEditorActionListener { _, actionId, _ ->
            if (actionId == IME_ACTION_SEARCH) {
                callService()
                activity?.let { Utils.hideKeyboard(it,binding.searchEditText) }
                return@OnEditorActionListener true
            }
            false
        })
    }

    /**
     *  Call service from viewMode
     *
     */
    private fun callService() {
        val name: String = binding.searchEditText.text.toString()
        if (name.isNotBlank()) {
            viewModel.foodInfo("under_30_minutes", "0", "7", name)
            binding.progress.visibility = View.VISIBLE
            binding.searchRecyclerview.visibility = View.GONE
            binding.noResultText.visibility = View.GONE
            //Log.e("search", name)
        } else {
            Toast.makeText(context, R.string.empty_files, Toast.LENGTH_SHORT).show()
        }
    }

    /**
     *
     * Listener for text edit text for show cross icon clicked
     *
     *
     */
    private fun editTextListener(){
        binding.searchEditText.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(editable: Editable?) {
                if (editable != null) {
                    if(editable.isNotEmpty()){
                        binding.closeButton.visibility = View.VISIBLE
                        //binding.searchButton.visibility = View.GONE
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })
    }

    /**
     * Clear text from edit text and hide icon
     *
     */
    private fun buttonClicked() {
            binding.closeButton.setOnClickListener {
                binding.searchEditText.setText("")
                binding.closeButton.visibility = View.GONE
                binding.searchRecyclerview.visibility = View.GONE

            }
    }
}