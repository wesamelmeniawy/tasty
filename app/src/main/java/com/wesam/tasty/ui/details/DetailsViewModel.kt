package com.wesam.tasty.ui.details

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.wesam.tasty.data.api.ApiRepository
import com.wesam.tasty.data.database.FoodRepository
import com.wesam.tasty.data.database.FoodRoomDatabase
import com.wesam.tasty.data.model.FoodEntity
import com.wesam.tasty.data.model.FoodModel
import kotlinx.coroutines.*
import javax.inject.Inject

class DetailsViewModel @Inject constructor(application: Application): ViewModel(){

    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.IO)

    private val repository: FoodRepository
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allFoods: LiveData<List<FoodEntity>>

    init {
        val foodsDao = FoodRoomDatabase.getDatabase(application, coroutineScope).foodDao()
        repository = FoodRepository(foodsDao)
        allFoods = repository.allFood
    }

    /**
     * Launching a new coroutine to update item to set favorite column
     *
     * @param id
     * @param favorite
     */
    fun update(id: Int,favorite:Boolean) = coroutineScope.launch(Dispatchers.IO) {
            repository.update(id,favorite)
    }


    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(foodEntity: FoodEntity) = coroutineScope.launch(Dispatchers.IO) {
        repository.insert(foodEntity)
    }


    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}