package com.wesam.tasty.ui.favorite

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.squareup.moshi.JsonAdapter
import com.wesam.tasty.adapter.SearchAdapter
import com.wesam.tasty.data.model.FoodEntity
import com.wesam.tasty.databinding.FragmentFavoriteBinding
import com.wesam.tasty.ui.details.DetailsActivity
import com.wesam.tasty.utils.Constants
import com.wesam.tasty.viewmodels.ViewModelProviderFactory
import dagger.android.support.DaggerFragment
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class FavoriteFragment : DaggerFragment(),SearchAdapter.ItemClickedListener {

    private lateinit var binding: FragmentFavoriteBinding
    private lateinit var viewModel: FavoriteViewModel


    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

    @Inject
    lateinit var requestManager: RequestManager


    private lateinit var favoriteAdapter: SearchAdapter

    private var favoriteList: List<FoodEntity> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentFavoriteBinding.inflate(inflater)
        viewModel = ViewModelProvider(this, providerFactory).get(FavoriteViewModel::class.java)

        handelRecyclerview()
        observeViewModel()
        return binding.root
    }

    /**
     *  Observe ViewModel
     *
     */
    private fun observeViewModel() {
        observeGetDataFromDatabase()
    }

    /**
     * Observe ViewModel to Get Data From Room Database
     *
     */
    private fun observeGetDataFromDatabase() {
        viewModel.favoriteFoods.observe(viewLifecycleOwner, Observer {
                Foods ->
            Foods?.let {
                if(Foods.isNotEmpty()) {
                    favoriteList = it
                    favoriteAdapter.loadData(favoriteList)
                }else{
                    binding.noResultText.visibility = View.VISIBLE
                }
            }
        })
    }

    /**
     * Handel recyclerview for setting adapter and layout
     *
     */
    private fun handelRecyclerview() {
        val dinnerLayoutManage = LinearLayoutManager(
            context, LinearLayoutManager.VERTICAL,
            false
        )
        favoriteAdapter = SearchAdapter(requestManager, this)
        binding.favoriteRecyclerview.layoutManager = dinnerLayoutManage
        binding.favoriteRecyclerview.adapter = favoriteAdapter
    }

    /**
     * Open activity details when item clicked from recyclerview
     * for showing more details about this item
     *
     * @param position
     */
    override fun onItemClickListener(position: Int) {
        val intent = Intent(context, DetailsActivity::class.java)
        val jsonAdapter: JsonAdapter<FoodEntity> = Constants.moshi.adapter(FoodEntity::class.java)
        intent.putExtra("food_details", jsonAdapter.toJson(favoriteList[position]))
        //Log.e("fffff",favoriteList[position].toString())
        startActivity(intent)
    }


}