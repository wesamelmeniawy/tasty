package com.wesam.tasty.ui.viewmore

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.squareup.moshi.JsonAdapter
import com.wesam.tasty.R
import com.wesam.tasty.adapter.FoodAdapter
import com.wesam.tasty.data.model.FoodEntity
import com.wesam.tasty.databinding.ActivityViewMoreBinding
import com.wesam.tasty.ui.details.DetailsActivity
import com.wesam.tasty.utils.Constants
import com.wesam.tasty.viewmodels.ViewModelProviderFactory
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class ViewMoreActivity : DaggerAppCompatActivity(),FoodAdapter.ItemClickedListener {
    private lateinit var binding: ActivityViewMoreBinding

    private lateinit var viewModel: ViewMoreViewModel

    private lateinit var viewMoreAdapter: FoodAdapter

    private var viewMoreList: List<FoodEntity> = ArrayList()

    private var type : String ? = ""


    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

    @Inject
    lateinit var requestManager: RequestManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this,R.layout.activity_view_more)
        viewModel = ViewModelProvider(this, providerFactory).get(ViewMoreViewModel::class.java)

        type = intent.getStringExtra("type")

        backButtonClicked()
        setHeaderName()
        handelRecyclerview()
        observeViewModel()
        callService()

    }

    /**
     *  Observe ViewModel
     *
     */
    private fun observeViewModel() {
       observeGetDinnerData()
       observeGetDessertData()
       observeGetHealthyData()
       observeGetFastFoodData()
        //observeGetDataFromDatabase()
    }

    /**
     * Observe ViewMode Get to Get Dinner Food Data and set it in array
     *
     */
    private fun observeGetDinnerData() {
        viewModel.foodModel.observe(this, Observer {
            if (it != null) {
                if(it.count != 0) {
                    viewMoreList = it.results!!
                    viewMoreAdapter.loadData(viewMoreList)
                }else{
                   // Log.e("null","null")
                    binding.progress.visibility = View.GONE
                }

            }else{
                //Log.e("null","null")
                binding.progress.visibility = View.GONE
            }
            binding.progress.visibility = View.GONE

        })
    }

    /**
     * Observe ViewMode Get to Get Dessert Food Data and set it in array
     *
     */
    private fun observeGetDessertData(){
        viewModel.dessertModel.observe(this, Observer {
            if (it != null) {
                if(it.count!=0) {
                    viewMoreList = it.results!!
                    viewMoreAdapter.loadData(viewMoreList)
                }else{
                    //Log.e("null","null")
                    binding.progress.visibility = View.GONE
                }
            }else {
                //Log.e("null","null")
                binding.progress.visibility = View.GONE
            }
            binding.progress.visibility = View.GONE
        })
    }

    /**
     *  Observe ViewMode Get to Get Fast Food Data and set it in array
     *
     */
    private fun observeGetFastFoodData(){
        viewModel.fastFoodModel.observe(this, Observer {
            if (it != null) {
                if(it.count != 0) {
                    viewMoreList = it.results!!
                    viewMoreAdapter.loadData(viewMoreList)
                }else{
                    //Log.e("null","null")
                    binding.progress.visibility = View.GONE
                }

            }else{
                //Log.e("null","null")
                binding.progress.visibility = View.GONE
            }
            binding.progress.visibility = View.GONE
        })
    }

    /**
     *  Observe ViewMode Get to Get Healthy Food Data and set it in array
     *
     */
    private fun observeGetHealthyData(){
        viewModel.healthyModel.observe(this, Observer {
            if (it != null) {
                if(it.count != 0) {
                    viewMoreList = it.results!!
                    viewMoreAdapter.loadData(viewMoreList)
                }else{
                    Toast.makeText(this,R.string.some_wrong,Toast.LENGTH_SHORT).show()
                    binding.progress.visibility = View.GONE
                }
            }else{
                Toast.makeText(this,R.string.some_wrong,Toast.LENGTH_SHORT).show()
                binding.progress.visibility = View.GONE
            }
            binding.progress.visibility = View.GONE
        })
    }
//    /**
//     * Observe ViewModel to Get Data From Room Database
//     *
//     */
//    private fun observeGetDataFromDatabase() {
//        viewModel.allFoods.observe(this, Observer { Foods ->
//            Foods?.let { Log.e("data", it.toString()) }
//        })
//    }

    /**
     *  Call service from viewMode
     *
     */
    private fun callService() {
        when(type){
            "dinner" -> viewModel.dinnerInfo("under_30_minutes", "0", "15", "dinner")
            "dessert" -> viewModel.dessertInfo("under_30_minutes", "0", "15", "dessert")
            "fast" -> viewModel.fastFoodInfo("under_30_minutes", "0", "15", "burger")
            "healthy" -> viewModel.healthyFoodInfo("under_30_minutes", "0", "15", "healthy")
        }
    }

    /**
     * Handel recyclerview for setting adapter and layout
     *
     */
    private fun handelRecyclerview(){
        val dinnerLayoutManage = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
            false)
        viewMoreAdapter = FoodAdapter(requestManager,this)
        binding.viewMoreRecyclerView.layoutManager = dinnerLayoutManage
        binding.viewMoreRecyclerView.adapter = viewMoreAdapter
    }

    /**
     *  Set header name dependence on food type
     *
     */
    private fun setHeaderName() {
        when(type){
            "dinner" -> binding.headerName.text = getString(R.string.dinner_dishes)
            "dessert" -> binding.headerName.text = getString(R.string.dessert_dishes)
            "fast" -> binding.headerName.text = getString(R.string.fast_food_dishes)
            "healthy" -> binding.headerName.text = getString(R.string.healthy_dishes)
        }
    }

    /**
     * Finish activity when clicked button
     *
     */
    private fun backButtonClicked(){
        binding.buttonBack.setOnClickListener {
            finish()
        }
    }

    /**
     * Open activity details when item clicked from recyclerview
     * for showing more details about this item
     *
     * @param position
     */
    override fun onItemClickListener(position: Int) {
        val intent = Intent(this,DetailsActivity::class.java)
        val jsonAdapter: JsonAdapter<FoodEntity> = Constants.moshi.adapter(FoodEntity::class.java)
        intent.putExtra("food_details",jsonAdapter.toJson(viewMoreList[position]))
        startActivity(intent)
    }
}