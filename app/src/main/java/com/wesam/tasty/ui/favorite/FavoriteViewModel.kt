package com.wesam.tasty.ui.favorite

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.wesam.tasty.data.database.FoodRepository
import com.wesam.tasty.data.database.FoodRoomDatabase
import com.wesam.tasty.data.model.FoodEntity
import kotlinx.coroutines.*
import javax.inject.Inject

class FavoriteViewModel @Inject constructor(application: Application): ViewModel() {



    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.IO)

    private val repository: FoodRepository

    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val favoriteFoods: LiveData<List<FoodEntity>>

    init {
        val foodsDao = FoodRoomDatabase.getDatabase(application, coroutineScope).foodDao()
        repository = FoodRepository(foodsDao)
        favoriteFoods = repository.favoriteFood
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}