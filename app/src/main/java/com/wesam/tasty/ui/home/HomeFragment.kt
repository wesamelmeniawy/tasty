package com.wesam.tasty.ui.home

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.RequestManager
import com.squareup.moshi.JsonAdapter
import com.wesam.tasty.R
import com.wesam.tasty.adapter.HomeAdapter
import com.wesam.tasty.data.model.FoodEntity
import com.wesam.tasty.databinding.FragmentHomeBinding
import com.wesam.tasty.ui.details.DetailsActivity
import com.wesam.tasty.ui.viewmore.ViewMoreActivity
import com.wesam.tasty.utils.Constants
import com.wesam.tasty.viewmodels.ViewModelProviderFactory
import dagger.android.support.DaggerFragment
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : DaggerFragment() ,HomeAdapter.ItemClickedListener{

    private lateinit var binding: FragmentHomeBinding

    private lateinit var viewModel: HomeViewModel

    private lateinit var dinnerAdapter: HomeAdapter
    private lateinit var healthyAdapter: HomeAdapter
    private lateinit var dessertAdapter: HomeAdapter
    private lateinit var fastFoodAdapter: HomeAdapter

    private var dinnerList: List<FoodEntity> = ArrayList()
    private var healthyList: List<FoodEntity> = ArrayList()
    private var dessertList: List<FoodEntity> = ArrayList()
    private var fastFoodList: List<FoodEntity> = ArrayList()
    private var foodListFromDB : List<FoodEntity> = ArrayList()
    private var dinnerFromDB: MutableList<FoodEntity> = mutableListOf()
    private var healthyFromDB: MutableList<FoodEntity>  = mutableListOf()
    private var dessertFromDB: MutableList<FoodEntity>  = mutableListOf()
    private var fastFoodFromDB: MutableList<FoodEntity>  = mutableListOf()



    @Inject
    lateinit var providerFactory: ViewModelProviderFactory

    @Inject
    lateinit var requestManager: RequestManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater)
        viewModel = ViewModelProvider(this, providerFactory).get(HomeViewModel::class.java)

        handelRecyclerview()
        observeViewModel()
        callService()
        clickedText()



        return binding.root
    }

    /**
     *  Observe ViewModel
     *
     */
    private fun observeViewModel() {
        observeGetDinnerData()
        observeGetDessertData()
        observeGetHealthyData()
        observeGetFastFoodData()
        observeGetDataFromDatabase()
    }

    /**
     * Observe ViewMode to Get Dinner Food Data , set it in adapter
     * and insert list of food to room database
     *
     */
    private fun observeGetDinnerData() {
        viewModel.foodModel.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if(it.count != 0) {
                    binding.dinnerTextView.visibility = View.VISIBLE
                    binding.dinnerViewMore.visibility = View.VISIBLE
                    dinnerList = it.results!!
                    dinnerAdapter.loadData(dinnerList)
                    var entity: FoodEntity
                    for (i in dinnerList) {
                        entity = FoodEntity(
                            i.id, i.name, i.image_url, i.description,
                            i.country, i.video_url, false, "dinner",i.section,i.instructions
                        )
                        //Log.e("entity",entity.toString())
                        observeInsertData(entity)
                    }
                }else{
                    setDataFromDB()
                    //dinnerAdapter.loadData(dinnerFromDB)
                    Toast.makeText(context, R.string.some_wrong,Toast.LENGTH_SHORT).show()
                binding.progress.visibility = View.GONE
            }

            }else{
                setDataFromDB()
                Toast.makeText(context, R.string.some_wrong,Toast.LENGTH_SHORT).show()
                //dinnerAdapter.loadData(dinnerFromDB)
                binding.progress.visibility = View.GONE
            }
            binding.progress.visibility = View.GONE

        })
    }

    /**
     * Observe ViewMode to Get Dessert Food Data , set it in adapter
     * and insert list of food to room database
     *
     */
    private fun observeGetDessertData(){
        viewModel.dessertModel.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if(it.count!=0) {
                    binding.dessertTextView.visibility = View.VISIBLE
                    binding.dessertViewMore.visibility = View.VISIBLE
                    dessertList = it.results!!
                    dessertAdapter.loadData(dessertList)
                    var entity: FoodEntity
                    for (i in dessertList) {
                        entity = FoodEntity(
                            i.id, i.name, i.image_url, i.description,
                            i.country, i.video_url, false, "dessert",i.section,i.instructions
                        )
                        observeInsertData(entity)
                    }
                }else{
                    setDataFromDB()
                    Toast.makeText(context, R.string.some_wrong,Toast.LENGTH_SHORT).show()
                    binding.progress.visibility = View.GONE
                }
            }else {
                setDataFromDB()
                Toast.makeText(context, R.string.some_wrong,Toast.LENGTH_SHORT).show()
                binding.progress.visibility = View.GONE
            }
            binding.progress.visibility = View.GONE
        })
    }

    /**
     *  Observe ViewMode to Get Fast Food Data , set it in adapter
     * and insert list of food to room database
     *
     */
    private fun observeGetFastFoodData(){
        viewModel.fastFoodModel.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if(it.count != 0) {
                    binding.fastFoodTextView.visibility = View.VISIBLE
                    binding.fastViewMore.visibility = View.VISIBLE
                    fastFoodList = it.results!!
                    fastFoodAdapter.loadData(fastFoodList)
                    var entity: FoodEntity
                    for (i in fastFoodList) {
                        entity = FoodEntity(
                            i.id, i.name, i.image_url, i.description,
                            i.country, i.video_url, false, "fast_food",i.section,i.instructions
                        )
                        observeInsertData(entity)
                    }
                }else{
                    setDataFromDB()
                    Toast.makeText(context, R.string.some_wrong,Toast.LENGTH_SHORT).show()
                    binding.progress.visibility = View.GONE
                }

            }else{
                setDataFromDB()
                Toast.makeText(context, R.string.some_wrong,Toast.LENGTH_SHORT).show()
                binding.progress.visibility = View.GONE
            }
            binding.progress.visibility = View.GONE
        })
    }

    /**
     *  Observe ViewMode to Get Healthy Food Data , set it in adapter
     * and insert list of food to room database
     *
     */
    private fun observeGetHealthyData(){
        viewModel.healthyModel.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                if(it.count != 0) {
                    binding.healthyTextView.visibility = View.VISIBLE
                    binding.healthyViewMore.visibility = View.VISIBLE
                    healthyList = it.results!!
                    healthyAdapter.loadData(healthyList)
                    var entity: FoodEntity
                    for (i in healthyList) {
                        entity = FoodEntity(
                            i.id, i.name, i.image_url, i.description,
                            i.country, i.video_url, false, "healthy",i.section,i.instructions
                        )
                        observeInsertData(entity)
                    }
                }else{
                    setDataFromDB()
                    Toast.makeText(context, R.string.some_wrong,Toast.LENGTH_SHORT).show()
                    binding.progress.visibility = View.GONE
                }
            }else{
                setDataFromDB()
                Toast.makeText(context, R.string.some_wrong,Toast.LENGTH_SHORT).show()
                binding.progress.visibility = View.GONE
            }
            binding.progress.visibility = View.GONE
        })
    }

    /**
     * Observe ViewModel to Get Data From Room Database
     *
     */
    private fun observeGetDataFromDatabase() {
        viewModel.allFoods.observe(viewLifecycleOwner, Observer { Foods ->
            Foods?.let {
                foodListFromDB = it }
        })
    }

    /**
     *  Insert data which get from api to room database
     *
     * @param foodEntity
     */
    private fun observeInsertData(foodEntity: FoodEntity) {
        viewModel.insert(foodEntity)
    }

    /**
     *  Call service from viewMode
     *
     */
    private fun callService() {
        viewModel.dinnerInfo("under_30_minutes", "0", "5", "dinner")
        viewModel.dessertInfo("under_30_minutes", "0", "5", "dessert")
        viewModel.healthyFoodInfo("under_30_minutes", "0", "5", "healthy")
        viewModel.fastFoodInfo("under_30_minutes", "0", "5", "burger")
    }

    /**
     * Handel recyclerview for setting adapter and layout
     *
     */
    private fun handelRecyclerview(){
        val dinnerLayoutManage = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,
            false)
        val dessertLayoutManage = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,
            false)
        val healthyLayoutManage = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,
            false)
        val fastLayoutManage = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,
            false)

        dinnerAdapter = HomeAdapter(requestManager,this,"dinner")
        binding.dinnerRecyclerview.layoutManager = dinnerLayoutManage
        binding.dinnerRecyclerview.adapter = dinnerAdapter

        dessertAdapter = HomeAdapter(requestManager,this,"dessert")
        binding.dessertRecyclerview.layoutManager = dessertLayoutManage
        binding.dessertRecyclerview.adapter = dessertAdapter

        healthyAdapter = HomeAdapter(requestManager,this,"healthy")
        binding.healthyRecyclerview.layoutManager = healthyLayoutManage
        binding.healthyRecyclerview.adapter = healthyAdapter

        fastFoodAdapter = HomeAdapter(requestManager,this,"fast_food")
        binding.fastFoodRecyclerview.layoutManager = fastLayoutManage
        binding.fastFoodRecyclerview.adapter = fastFoodAdapter
    }

    /**
     * Clicked TextView [View More] to show more item
     *
     */
    private fun clickedText(){
        binding.dinnerViewMore.setOnClickListener {
            val intent = Intent(context,
                ViewMoreActivity::class.java)
            intent.putExtra("type","dinner")
            startActivity(intent)
        }
        binding.dessertViewMore.setOnClickListener {
            val intent = Intent(context,
                ViewMoreActivity::class.java)
            intent.putExtra("type","dessert")
            startActivity(intent)
        }
        binding.fastViewMore.setOnClickListener {
            val intent = Intent(context,
                ViewMoreActivity::class.java)
            intent.putExtra("type","fast")
            startActivity(intent)
        }
        binding.healthyViewMore.setOnClickListener {
            val intent = Intent(context,
                ViewMoreActivity::class.java)
            intent.putExtra("type","healthy")
            startActivity(intent)
        }
    }

    /**
     * Open activity details when item clicked from recyclerview
     * for showing more details about this item
     *
     * @param position
     */
    override fun onItemClickListener(position: Int,type:String) {
        val intent = Intent(context, DetailsActivity::class.java)
        val jsonAdapter: JsonAdapter<FoodEntity> = Constants.moshi.adapter(FoodEntity::class.java)

        if(dinnerList.isNotEmpty() || dessertList.isNotEmpty()
            || healthyList.isNotEmpty() || fastFoodList.isNotEmpty()) {
            when (type) {
                "dinner" -> {
                    intent.putExtra("food_details", jsonAdapter.toJson(dinnerList[position]))
                }
                "dessert" -> {
                    intent.putExtra("food_details", jsonAdapter.toJson(dessertList[position]))
                }
                "fast_food" -> {
                    intent.putExtra("food_details", jsonAdapter.toJson(fastFoodList[position]))
                }
                "healthy" -> {
                    intent.putExtra("food_details", jsonAdapter.toJson(healthyList[position]))
                }
            }
        }else{
            when (type) {
                "dinner" -> {
                    intent.putExtra("food_details", jsonAdapter.toJson(dinnerFromDB[position]))
                }
                "dessert" -> {
                    intent.putExtra("food_details", jsonAdapter.toJson(dessertFromDB[position]))
                }
                "fast_food" -> {
                    intent.putExtra("food_details", jsonAdapter.toJson(fastFoodFromDB[position]))
                }
                "healthy" -> {
                    intent.putExtra("food_details", jsonAdapter.toJson(healthyFromDB[position]))
                }
            }
        }

        startActivity(intent)
    }

    /**
     * Set data to adapter from room db
     *
     */
    private fun setDataFromDB(){
        for(i in foodListFromDB){
            dinnerFromDB.remove(i)
            dessertFromDB.remove(i)
            fastFoodFromDB.remove(i)
            healthyFromDB.remove(i)
            when(i.type){
                "dinner" ->{dinnerFromDB.add(i)
                dinnerAdapter.loadData(dinnerFromDB)
                }
                "dessert"->{dessertFromDB.add(i)
                dessertAdapter.loadData(dessertFromDB)}
                "fast_food"->{fastFoodFromDB.add(i)
                fastFoodAdapter.loadData(fastFoodFromDB)}
                "healthy"->{healthyFromDB.add(i)
                healthyAdapter.loadData(healthyFromDB)}
            }
        }
    }

}