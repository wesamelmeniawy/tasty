package com.wesam.tasty.utils

import com.squareup.moshi.Moshi

object Constants {
    private const val BASE_URL = "https://tasty.p.rapidapi.com"
    internal const val API_URL = "$BASE_URL/"

    val moshi = Moshi.Builder().build()
}