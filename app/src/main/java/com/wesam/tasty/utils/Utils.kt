package com.wesam.tasty.utils

import android.app.Activity
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.content.ContextCompat.getSystemService
import com.wesam.tasty.R


object Utils{
    /**
     * hide keyboard
     *
     * @param activity
     */
    fun hideKeyboard(activity:Activity,editText: EditText){
        val imm: InputMethodManager? =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(activity.currentFocus?.windowToken, 0)

    }

}
