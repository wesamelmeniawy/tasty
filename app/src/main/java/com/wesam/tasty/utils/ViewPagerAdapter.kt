package com.wesam.tasty.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
) {

    private val mFragmentTitleList = ArrayList<String>()
    private val mFragmentList = ArrayList<Fragment>()

    /**
     * getItem
     * @param position
     * @return
     */
    override fun getItem(position : Int): Fragment {
        return mFragmentList[position]
    }

    /**
     * getCount
     * @return
     */
    override fun getCount():Int{
        return mFragmentList.size
    }

    /**
     * addFragment
     * @param fragment
     * @param title
     */
    fun addFragment(fragment : Fragment, title : String){
        mFragmentList.add(fragment)
        mFragmentTitleList.add(title)
    }

    /**
     * getPageTitle
     * @param position
     * @return
     */
    override fun getPageTitle(position : Int):CharSequence{
        return mFragmentTitleList[position]
    }
}